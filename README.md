# createAppIcon

#### 介绍

Linux系统为压缩包应用生成开始菜单

#### 安装教程

##### jar文件安装

1. java运行环境在1.8+；

2. 使用java -jar ***.jar运行程序。

##### deb文件安装

1. 适用于debain、ubuntu等系统；

2. 使用命令 sudo dpkg -i ***.deb安装程序；

3. 运行程序。

#### 使用说明
##### 手动生成
1. 程序界面

![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/214615_b3fdc26a_1371177.png "屏幕截图.png")

    1.1 在第一行输入程序需要在开始菜单显示的名称；

    1.2 在第二行输入程序执行文件全路径；

    1.3 在第三行输入程序的图标全路径；

    1.4 点击开始生成按钮在开始菜单生成应用图标。

##### 自动生成
1. 主界面

![输入图片说明](resource/imgs/截图录屏_Start_20201129102326.png)

2. 点击左上角生成方式，选择自动生成

![输入图片说明](resource/imgs/截图录屏_Start_20201129102547.png)

3. 选择应用文件，程序会自动生成文件名；
4. 选择应用图标文件，点击开始生成即可。
